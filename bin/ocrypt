#!/usr/bin/python3 -s

import argparse
import argcomplete
from os import path

import uno_util
from uno_util import office
from uno_util import document
from uno_util import util

parser = argparse.ArgumentParser(description = uno_util.__doc__.strip())
subparsers = parser.add_subparsers(required = True, dest = 'cmd')

def add_parser(subcommand, help, epilog = None):

    sub_parser = subparsers.add_parser(subcommand.__name__.replace('_', '-'),
      help = help, epilog = epilog)
    sub_parser.set_defaults(subcommand = subcommand)
    return sub_parser

sub_parser = add_parser(document.decrypt, 'Decrypts a single open document file.')
sub_parser = add_parser(document.encrypt, 'Encrypts a single open document file.')
sub_parser = add_parser(document.decrypt_libs, 'Decrypts all non-standard basic libraries in a single open document file.')
sub_parser = add_parser(document.encrypt_libs, 'Encrypts all non-standard basic libraries in a single open document file.')
parser.add_argument('passwd', metavar = 'PASSWORD', help = 'file containing password')
parser.add_argument('src', metavar = 'SOURCE', help = 'file to read')
parser.add_argument('dst', metavar = 'DESTINATION', help = 'file to write')

argcomplete.autocomplete(parser)

args = parser.parse_args()

with open(args.passwd) as reader:
    passwd = reader.read().rstrip()

exit_code = None
with office.fresh_context() as ctxt:
    desktop = util.get_desktop(ctxt)
    exit_code = args.subcommand(desktop, passwd, args.src, args.dst)

if not path.exists(args.dst):
    raise Exception('Operation failure: did not produce destination file {}'.format(args.dst))

if exit_code is not None and exit_code != 0:
    raise SystemExit(exit_code)
