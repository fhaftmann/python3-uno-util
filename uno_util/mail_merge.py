
'''Mail merge.'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)


__all__ = ['execute', 'to_pdf_files']


from os import path
import tempfile
import os
import shutil

import uno

from . import util


def execute(ctxt, doc, **opts):

    settings = doc.createInstance('com.sun.star.text.DocumentSettings')

    mail_merge = util.create_uno_service(ctxt, 'com.sun.star.text.MailMerge',
      DocumentURL = doc.Location,
      DataSourceName = settings.CurrentDatabaseDataSource,
      CommandType = settings.CurrentDatabaseCommandType,
      Command = settings.CurrentDatabaseCommand,
      **opts)

    mail_merge.execute(())

    mail_merge.dispose()


def to_pdf_files(ctxt, doc, dst, filename_column = None, **opts):

    if not path.isdir(dst):
        raise NotADirectoryError(dst)

    if filename_column is not None:
        opts.update(dict(FileNamePrefix = filename_column,
          FileNameFromColumn = True))

    with tempfile.TemporaryDirectory(prefix = 'mail_merge.') as dst_tmp:
        ## using an empty directory avoids funny renaming policies for generated pdfs
        execute(ctxt, doc,
          OutputURL = util.urlify(dst_tmp),
          OutputType = uno.getConstantByName('com.sun.star.text.MailMergeType.FILE'),
          SaveFilter = 'writer_pdf_Export',
          **opts)
        names = []
        for name in os.listdir(dst_tmp):
            src = path.join(dst_tmp, name)
            if path.isfile(src):
                shutil.move(path.join(dst_tmp, name), path.join(dst, name))
                names.append(name)

    return names
