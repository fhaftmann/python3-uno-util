
'''UNO documents.'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)


from contextlib import contextmanager
from os import path

from . import util


__all__ = ['load', 'save', 'opening',
  'decrypt', 'encrypt', 'decrypt_libs', 'encrypt_libs', 'create_pdf']


std_lib = 'Standard'

pdf_filters = {
  'com.sun.star.text.TextDocument': 'writer_pdf_Export',
  'com.sun.star.sheet.SpreadsheetDocument': 'calc_pdf_Export',
  'com.sun.star.drawing.DrawingDocument': 'draw_pdf_Export',
  'com.sun.star.presentation.PresentationDocument': 'impress_pdf_Export',
}


class Failed_Load(Exception):
    pass


def load(desktop, loc, **props):

    if not path.exists(loc):
        raise FileNotFoundError(loc)
    url = util.urlify(loc)
    doc = desktop.loadComponentFromURL(url, "_blank", 0, util.props(**props))
    if doc is None:
        raise Failed_Load(url)
    return doc


def save(doc, loc, **props):

    doc.storeToURL(util.urlify(loc), util.props(**props))


@contextmanager
def opening(desktop, loc):

    doc = load(desktop, loc)
    try:
        yield doc
    finally:
        doc.close(True)


def change_library_password(doc, old, new):

    lib_container = doc.BasicLibraries
    for lib_name in lib_container.ElementNames:
        if lib_name == std_lib:
            continue
        lib_container.setLibraryReadOnly(lib_name, False)
        lib_container.changeLibraryPassword(lib_name, old, new)


def decrypt(desktop, passwd, src, dst):

    doc = load(desktop, src, Password = passwd)
    save(doc, dst)
    doc.dispose()
    doc.close(True)


def encrypt(desktop, passwd, src, dst):

    doc = load(desktop, src)
    save(doc, dst, Password = passwd)
    doc.dispose()
    doc.close(True)


def decrypt_libs(desktop, passwd, src, dst):

    doc = load(desktop, src)
    change_library_password(doc, passwd, '')
    save(doc, dst)
    doc.dispose()
    doc.close(True)


def encrypt_libs(desktop, passwd, src, dst):

    doc = load(desktop, src)
    change_library_password(doc, '', passwd)
    save(doc, dst)
    doc.dispose()
    doc.close(True)


def create_pdf(desktop, src, dst, passwd = None):

    doc = load(desktop, src)
    identifier = doc.getIdentifier()
    pdf_filter = pdf_filters[identifier]
    save(doc, dst, FilterName = pdf_filter,
      FilterData = util.propss(DocumentOpenPassword = passwd or '', EncryptFile = True))
    doc.dispose()
    doc.close(True)
