
'''Simple conversions between document files.'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)


__all__ = ['to_format', 'to_pdf', 'to_csvs']


from pathlib import Path
from os import path
import tempfile
import shutil
import time

from . import office


def extension_of_format(format):

    return format.split(':')[0]


def to_format(format, src, dst = None, src_format = None):

    if not path.isfile(src):
        raise FileNotFoundError(src)

    ext = '.' + extension_of_format(format)
    proper_dst = dst if dst is not None \
      else path.splitext(src)[0] + ext

    with tempfile.TemporaryDirectory() as workbench:
        args = ([] if src_format is None else [f'--infilter={src_format}']) \
          + ['--convert-to', format, '--outdir', workbench, src]
        office.batch_run(*args)
        tmp_dst = path.join(workbench, path.splitext(path.basename(src))[0] + ext)
        while not path.exists(tmp_dst):
            time.sleep(0.1)
              ## result seems to appear asynchronously
        shutil.move(tmp_dst, proper_dst)


def to_pdf(src, dst = None):

    to_format('pdf', src, dst)


def csv_export_format():

    ## cf. https://help.libreoffice.org/latest/en-GB/text/shared/guide/csv_params.html
    options = [
      '44', ## comma
      '34', ## quote
      '76', ## utf-8
      '1', ## start row
      '', ## cell format
      '', ## language identifier
      'false', ## quote
      'false', ## numbers
      'false', ## cell contents as showns
      'false', ## formulas
      'false', ## trim leading and trailing spaces
      '-1', ## all sheets
      'false' ## evaluate formulas on inpurt
    ]
    return f'csv:Text - txt - csv (StarCalc):{",".join(options)}'


def to_csvs(src, dst, src_format = None):

    src = Path(src)
    dst = Path(dst)

    if not src.is_file():
        raise FileNotFoundError(src)

    dst.mkdir()

    format

    args = ([] if src_format is None else [f'--infilter={src_format}']) \
      + ['--convert-to', csv_export_format(), '--outdir', dst, src]
    office.batch_run(*args)
