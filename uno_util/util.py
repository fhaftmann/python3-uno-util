
'''UNO utilities.'''

# Author: Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
# Licensed under the GNU General Public License v2 (GPLv2)


from os import path

import uno
from com.sun.star.beans import PropertyValue


__all__ = ['props', 'propss', 'urlify',
  'create_uno_service', 'get_desktop',
  'show_interface_docs', 'show_service_docs']


def props(**d):

    def mk(key, value):
        prop = PropertyValue()
        prop.Name = key
        prop.Value = value
        return prop

    return tuple([mk(key, value) for key, value in d.items()])


def propss(**d):

    return uno.Any("[]com.sun.star.beans.PropertyValue",
      tuple(props(**d)))


def urlify(loc):

    return uno.systemPathToFileUrl(path.realpath(loc))


def create_uno_service(ctxt, name, **opts):

    service = ctxt.ServiceManager.createInstanceWithContext(name, ctxt)
    for name in opts:
        setattr(service, name, opts[name])
    return service


def get_desktop(ctxt):

    return create_uno_service(ctxt, 'com.sun.star.frame.Desktop')


def get_service_documenter(ctxt):

    return ctxt.getValueByName('/singletons/com.sun.star.util.theServiceDocumenter')


def show_interface_docs(ctxt, x):

    get_service_documenter(ctxt).showInterfaceDocs(x)


def show_service_docs(ctxt, x):

    get_service_documenter(ctxt).showServiceDocs(x)
